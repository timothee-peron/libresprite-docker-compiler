FROM debian:latest

RUN apt-get update \
&& apt-get install cmake g++ make ninja-build git curl python -y \
&& apt-get install -y \
libfontconfig1-dev libfreetype6-dev libgif-dev libgl1-mesa-dev libgtest-dev libharfbuzz-dev libjpeg-dev libpixman-1-dev libpng-dev libtinyxml-dev libx11-dev libxcursor-dev libxi-dev mesa-common-dev zlib1g-dev libcurl4-gnutls-dev liblua5.4-dev libpng-dev libsdl2-dev libsdl2-image-dev libnode-dev


# dl and build of skia (aseprite-m81 branch)

#RUN git clone https://github.com/aseprite/skia.git \
RUN mkdir /deps \
&& cd /deps \
&& git clone https://chromium.googlesource.com/chromium/tools/depot_tools.git \
&& git clone -b aseprite-m81 https://github.com/aseprite/skia.git \
&& export PATH="${PWD}/depot_tools:${PATH}" \
&& cd skia \
&& python tools/git-sync-deps \
&& gn gen out/Release-x64 --args="is_debug=false is_official_build=true skia_use_system_expat=false skia_use_system_icu=false skia_use_system_libjpeg_turbo=true skia_use_system_libpng=true skia_use_system_libwebp=false skia_use_system_zlib=true" \
&& ninja -C out/Release-x64 skia modules

# dl and build libresprite

RUN git clone --recursive https://github.com/LibreSprite/LibreSprite \
&& cd LibreSprite \
&& mkdir build \
&& cd build \
&& cmake \
  -Wno-dev \
  -DCMAKE_BUILD_TYPE=RelWithDebInfo \
  -DLAF_BACKEND=skia \
  -DSKIA_DIR=/deps/skia \
  -DSKIA_LIBRARY_DIR=/deps/skia/out/Release-x64 \
  -DSKIA_LIBRARY=/deps/skia/out/Release-x64/libskia.a \
  -G Ninja \
  .. \
&& ninja libresprite

# check executable(optional)

# RUN ls -lah /LibreSprite/build/bin/
