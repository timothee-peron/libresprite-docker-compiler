#!/bin/sh

k=0;
dockerTag=libresprite-compiler;

help() {
  echo "Docked compiler script!";
  echo "Available options:";
  echo " --help -h       show this help";
  echo " -k              discard cached containers by using docker build with --no-cache --rm=true";
  echo " -clean          don't build, clean built files";
  exit 0;
}

clean(){
  docker rmi $dockerTag;
  exit 0;
}

while [ $# -gt 0 ] ; do
  case "$1" in
    "-k")                 k=1; break;;
    "-clean")             clean;;
    "--help"|"-h")        help;;
    *)                    echo "Argument $1 is not accepted!"; exit 1;;
  esac
  shift;
done

if [ $k -eq 0 ]; then
  cacheParam="--no-cache --rm=true";
fi

docker build $cacheParam -t $dockerTag .
docker run -v "$(pwd)"/bin:/output/bin -it $dockerTag cp /LibreSprite/build/bin /output -r

cp "$(pwd)"/libresprite.ico "$(pwd)"/bin/libreprite.ico

